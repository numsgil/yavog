// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Octree/VoxelBranch.h"
#include "Octree/VoxelLeaf.h"
#include "VoxelOctreeActor.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOctreeReady);

UCLASS()
class YAVOG_API AVoxelOctreeActor : public AActor
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Voxel Octree")
	int32 HalfSideLength = 32;

	UPROPERTY(BlueprintReadOnly, Category = "Voxel Octree")
	UVoxelBranch* OctreeRoot;

	UPROPERTY(BlueprintAssignable, Category = "Voxel Octree")
	FOctreeReady ReadyEvent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Voxel Octree")
	class UMaterialInterface* Material = nullptr;

	// Sets default values for this actor's properties
	AVoxelOctreeActor();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick(float DeltaSeconds) override;

protected:
	virtual void AddVoxelBranch(UVoxelBranch* Branch);

	UPROPERTY()
	TArray<UVoxelLeaf*> Leaves;

	UPROPERTY()
	int32 LeafIndex = 0;
};
