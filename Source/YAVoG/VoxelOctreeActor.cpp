// Fill out your copyright notice in the Description page of Project Settings.

#include "YAVoG.h"
#include "VoxelOctreeActor.h"
#include "VoxelMeshActor.h"
#include "GenerateFace.h"

// Sets default values
AVoxelOctreeActor::AVoxelOctreeActor() : OctreeRoot(nullptr)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneNode"));
}

// Called when the game starts or when spawned
void AVoxelOctreeActor::BeginPlay()
{
	Super::BeginPlay();

	OctreeRoot = NewObject<UVoxelBranch>();
	OctreeRoot->Initialize(HalfSideLength, FInt3(0, 0, 0));
	OctreeRoot->Generate();
	AddVoxelBranch(OctreeRoot);
}

// Called every frame
void AVoxelOctreeActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (LeafIndex < Leaves.Num())
	{
		FActorSpawnParameters Parameters;
		Parameters.Owner = this;
		Parameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

		int32 i;
		for (i = 0; i < 8 && LeafIndex + i < Leaves.Num(); i++)
		{
			UVoxelLeaf* Child = Leaves[LeafIndex + i];

			AVoxelMeshActor* Mesh = GetWorld()->SpawnActor<AVoxelMeshActor>(GetActorLocation() + (Child->Origin.ToFloat() * Size), FRotator(0.f, 0.f, 0.f), Parameters);
			if (Mesh != nullptr)
			{
				Mesh->Mesh->SetMaterial(0, Material);
				Mesh->Node = Child;
			}
		}

		LeafIndex += i;
		if (LeafIndex >= Leaves.Num()) ReadyEvent.Broadcast();
	}
}

void AVoxelOctreeActor::AddVoxelBranch(UVoxelBranch* Branch)
{
	check(Branch != nullptr);
	for (int32 i = 0; i < 8; i++)
	{
		if (Branch->Children[i] != nullptr)
		{
			UVoxelNode* Child = Branch->Children[i];
			if (Child->IsLeaf())
				Leaves.Add(Cast<UVoxelLeaf>(Child));
			else
				AddVoxelBranch(Cast<UVoxelBranch>(Child));
		}
	}
}
