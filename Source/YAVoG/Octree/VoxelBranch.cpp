// Fill out your copyright notice in the Description page of Project Settings.

#include "../YAVoG.h"
#include "VoxelBranch.h"
#include "VoxelLeaf.h"

int32 UVoxelBranch::GetOctantContainingVoxel(FInt3 const& Point, FInt3 const& Origin)
{
	return 0 | (Point.X >= Origin.X ? 4 : 0) | (Point.Y >= Origin.Y ? 2 : 0) | (Point.Z >= Origin.Z ? 1 : 0);
}

UVoxelBranch::UVoxelBranch()
{ }

UVoxelBranch::~UVoxelBranch()
{ }

void UVoxelBranch::Initialize(int32 NewHalfSideLength, FInt3 const& NewOrigin)
{
	check(NewHalfSideLength > 8);
	Super::Initialize(NewHalfSideLength, NewOrigin);

	for (int32 i = 0; i < 8; i++)
		Children[i] = nullptr;
}

void UVoxelBranch::Generate()
{
	check(IsInitialized);
	for (int32 i = 0; i < 8; i++)
	{
		if (Children[i] == nullptr)
		{
			FInt3 NewOrigin = Origin + FInt3(
				HalfSideLength / (i & 4 ? 2 : -2),
				HalfSideLength / (i & 2 ? 2 : -2),
				HalfSideLength / (i & 1 ? 2 : -2));
			int32 NewHalfSideLength = HalfSideLength / 2;
			Children[i] = NewHalfSideLength > 8 ? (UVoxelNode*)NewObject<UVoxelBranch>() : (UVoxelNode*)NewObject<UVoxelLeaf>();
			Children[i]->Parent = this;
			Children[i]->Initialize(NewHalfSideLength, NewOrigin);
		}

		Children[i]->Generate();

		if (Children[i]->IsEmpty())
		{
			Children[i]->ConditionalBeginDestroy();
			Children[i] = nullptr;
		}
	}
}

bool UVoxelBranch::IsEmpty() const
{
	bool Empty = true;
	for (int32 i = 0; i < 8; i++)
		if (Children[i] != nullptr && !Children[i]->IsEmpty())
			Empty = false;
	return Empty;
}

bool UVoxelBranch::IsDirty() const
{
	bool Dirty = false;
	for (int32 i = 0; i < 8; i++)
		if (Children[i] != nullptr && Children[i]->IsDirty())
			Dirty = true;
	return Dirty;
}

void UVoxelBranch::SetVoxelAt(FInt3 const& Position, int32 Value)
{
	check(IsInitialized);
	check(Value >= 0 && Value <= MAX_uint16);
	int32 octant = GetOctantContainingVoxel(Position, Origin);

	if (Children[octant] == nullptr && Value == 0) return;

	if (Children[octant] == nullptr)
	{
		FInt3 NewOrigin = Origin + FInt3(
			HalfSideLength / (octant & 4 ? 2 : -2),
			HalfSideLength / (octant & 2 ? 2 : -2),
			HalfSideLength / (octant & 1 ? 2 : -2));
		int32 NewHalfSideLength = HalfSideLength / 2;
		Children[octant] = NewHalfSideLength > 8 ? (UVoxelNode*)NewObject<UVoxelBranch>() : (UVoxelNode*)NewObject<UVoxelLeaf>();
		Children[octant]->Parent = this;
		Children[octant]->Initialize(NewHalfSideLength, NewOrigin);
	}

	Children[octant]->SetVoxelAt(Position, Value);

	if (Children[octant]->IsEmpty())
	{
		Children[octant]->ConditionalBeginDestroy();
		Children[octant] = nullptr;
	}
}

int32 UVoxelBranch::GetVoxelAt(FInt3 const& Position) const
{
	check(IsInitialized);
	//if (Position.X > HalfSideLength || Position.Y > HalfSideLength || Position.Z > HalfSideLength) return 0;
	int32 octant = GetOctantContainingVoxel(Position, Origin);

	if (Children[octant] == nullptr) return 0;
	return Children[octant]->GetVoxelAt(Position);
}
