// Fill out your copyright notice in the Description page of Project Settings.

#include "YAVoG.h"
#include "VoxelGenerator.h"

#include "ScalePointModule.h"
#include "ScaleBiasModule.h"
#include "PerlinModule.h"

UVoxelGenerator::UVoxelGenerator() : HeightMap(nullptr), Underground(nullptr)
{
}

UVoxelGenerator::~UVoxelGenerator()
{
}

void UVoxelGenerator::Initialize(int32 Seed)
{
	if (HeightMap != nullptr)
	{
		HeightMap->ConditionalBeginDestroy();
		HeightMap = nullptr;
	}

	if (Underground != nullptr)
	{
		Underground->ConditionalBeginDestroy();
		Underground = nullptr;
	}

	{
		auto perlin = NewObject<UPerlinModule>();
		perlin->Seed = Seed;
		perlin->Lacunarity = 1.75f;

		auto scale = NewObject<UScalePointModule>();
		scale->XScale = 1 / 100.f;
		scale->YScale = 1 / 100.f;
		scale->ZScale = 1 / 100.f;
		scale->SetSourceModule(0, perlin);

		auto bias = NewObject<UScaleBiasModule>();
		bias->Scale = 32.f;
		bias->Bias = -16.f;
		bias->SetSourceModule(0, scale);

		HeightMap = bias;
	}
}

int32 UVoxelGenerator::GetHeightAt(int32 X, int32 Y) const
{
	check(HeightMap != nullptr);
	return FMath::FloorToInt(HeightMap->GetValue(X, Y, 0));
}
