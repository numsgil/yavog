// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Object.h"
#include "IntMath.h"
#include "VoxelGenerator.generated.h"

UCLASS(BlueprintType)
class YAVOG_API UVoxelGenerator : public UObject
{
	GENERATED_BODY()

public:
	UVoxelGenerator();
	virtual ~UVoxelGenerator();

	UFUNCTION(BlueprintCallable, Category = "Voxel Generator")
	virtual void Initialize(int32 Seed);

	UFUNCTION(BlueprintCallable, Category = "Voxel Generator")
	virtual int32 GetHeightAt(int32 X, int32 Y) const;

protected:
	UPROPERTY()
	class UNoiseModule* HeightMap;

	UPROPERTY()
	class UNoiseModule* Underground;
};
